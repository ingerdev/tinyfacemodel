#pragma once
#pragma warning(push, 0) 
#include <exception>
#pragma warning(pop) 
namespace videorender
{
	namespace internal
	{
		enum class VideoDecoderError
		{
			CannotOpenFile,
			CannotFindProperCodec,
			CannotFindBestStream,
			CannotFindStreamInfo,
			CannotCreateSwsContext,
			CannotFillFramedata,
			CannotAllocateFrame,
			CannotAllocatePacket,
			CannotCopyDecoderParameters,
			UnknownError,


		};
		class VideoDecoderException : public std::exception
		{
		public:
			VideoDecoderException(VideoDecoderError e) :Error(e) {}

			virtual char const * what() const { return "Something bad happend."; }
			VideoDecoderError Error;
		};

		enum class CameraError
		{
			CannotOpenDevice
		};

		class CameraException : public std::exception
		{
		public:
			CameraException(CameraError e) :Error(e) {}

			virtual char const * what() const { return "Something bad happend."; }
			CameraError Error;
		};
	}
}

